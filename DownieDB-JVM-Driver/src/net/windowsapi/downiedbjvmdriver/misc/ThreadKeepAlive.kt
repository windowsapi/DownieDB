package net.windowsapi.downiedbjvmdriver.misc

/**
 *
 * <p>===================================</p>
 * <p>Project: DownieDB</p>
 * <p>Package: net.windowsapi.downiedbjvmdriver.misc</p>
 * <p>File: ThreadKeepAlive</p>
 * <p>===================================</p>
 * <p>Author: Robin F.
 * <p>===================================</p>
 * <p>Date: 23.08.2017
 * <p>Time: 03:18
 * <p>===================================</p>
 *
 */
class ThreadKeepAlive : Thread() {

    override fun run() {
        while (!isInterrupted) {
        }
    }
}