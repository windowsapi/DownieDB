package net.windowsapi.downiedbjvmdriver.networking

import com.esotericsoftware.kryonet.Connection
import com.esotericsoftware.kryonet.Listener
import net.windowsapi.downiedb.sys.networking.packet.Packet
import net.windowsapi.downiedb.sys.networking.packet.PacketDataResult
import net.windowsapi.downiedb.sys.networking.packet.PacketResultCode

/**
 *
 * <p>===================================</p>
 * <p>Project: DownieDB</p>
 * <p>Package: net.windowsapi.downiedbjvmdriver.networking</p>
 * <p>File: DownieDBJVMDriverListener</p>
 * <p>===================================</p>
 * <p>Author: Robin F.
 * <p>===================================</p>
 * <p>Date: 21.08.2017
 * <p>Time: 01:53
 * <p>===================================</p>
 *
 */
class DownieDBJVMDriverListener constructor(val downieDBConnection: DownieDBConnection) : Listener() {

    override fun received(connection: Connection?, received: Any?) {
        if (received is Packet) {

            if (received is PacketDataResult) {
                val result = downieDBConnection.results.poll()
                result.result = received.result

                downieDBConnection.results.remove(result)
            }
        }
    }
}