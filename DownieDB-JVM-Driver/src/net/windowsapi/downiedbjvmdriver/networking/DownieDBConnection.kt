package net.windowsapi.downiedbjvmdriver.networking

import com.esotericsoftware.kryonet.Client
import net.windowsapi.downiedb.sys.networking.packet.*
import net.windowsapi.downiedbjvmdriver.misc.ThreadKeepAlive
import java.io.Closeable
import java.util.*

/**
 *
 * <p>===================================</p>
 * <p>Project: DownieDB</p>
 * <p>Package: net.windowsapi.downiedbjvmdriver.networking</p>
 * <p>File: DownieDBConnection</p>
 * <p>===================================</p>
 * <p>Author: Robin F.
 * <p>===================================</p>
 * <p>Date: 21.08.2017
 * <p>Time: 01:47
 * <p>===================================</p>
 *
 */
class DownieDBConnection constructor(host: String = "localhost", port: Int = 17553) : Closeable {

    private val client = Client()
    private val threadKeepAlive = ThreadKeepAlive()

    val results: Queue<Result> = LinkedList<Result>()

    init {
        client.start()
        client.connect(5000, host, port)

        client.kryo.register(Packet::class.java)
        client.kryo.register(PacketCreateDatabase::class.java)
        client.kryo.register(PacketDataInsert::class.java)
        client.kryo.register(PacketDataRequest::class.java)
        client.kryo.register(PacketDataResult::class.java)
        client.kryo.register(PacketResultCode::class.java)

        client.addListener(DownieDBJVMDriverListener(this))

        threadKeepAlive.start()
    }

    fun createDatabase(name: String) {
        client.sendTCP(PacketCreateDatabase(name))
    }

    fun insertData(databaseName: String, key: String, toInsert: Any, override: Boolean = true) {
        client.sendTCP(PacketDataInsert(databaseName, key, toInsert, override))
    }

    fun requestData(databaseName: String, key: String): Any? {
        val result = Result()

        results.offer(result)
        client.sendTCP(PacketDataRequest(databaseName, key))

        while (result.result == null) {
            print("")
        }

        return result.result
    }

    override fun close() {
        threadKeepAlive.interrupt()
    }
}
