package net.windowsapi.downiedbjvmdriver.networking

/**
 *
 * <p>===================================</p>
 * <p>Project: DownieDB</p>
 * <p>Package: net.windowsapi.downiedbjvmdriver.networking</p>
 * <p>File: Result</p>
 * <p>===================================</p>
 * <p>Author: Robin F.
 * <p>===================================</p>
 * <p>Date: 21.08.2017
 * <p>Time: 02:13
 * <p>===================================</p>
 *
 */
class Result constructor(var result: Any? = null)