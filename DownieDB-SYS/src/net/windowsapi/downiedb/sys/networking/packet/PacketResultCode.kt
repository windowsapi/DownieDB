package net.windowsapi.downiedb.sys.networking.packet

import net.windowsapi.downiedb.sys.misc.Constants

/**
 *
 * <p>===================================</p>
 * <p>Project: DownieDB</p>
 * <p>Package: net.windowsapi.downiedb.networking.packet</p>
 * <p>File: PacketResultCode</p>
 * <p>===================================</p>
 * <p>Author: Robin F.
 * <p>===================================</p>
 * <p>Date: 21.08.2017
 * <p>Time: 00:53
 * <p>===================================</p>
 *
 */
class PacketResultCode constructor(val code: Int = Constants.CODE_SUCCESS) : PacketOut