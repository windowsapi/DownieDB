package net.windowsapi.downiedb.sys.networking.packet

/**
 *
 * <p>===================================</p>
 * <p>Project: DownieDB</p>
 * <p>Package: net.windowsapi.downiedb.networking.packet</p>
 * <p>File: PacketCreateDatabase</p>
 * <p>===================================</p>
 * <p>Author: Robin F.
 * <p>===================================</p>
 * <p>Date: 20.08.2017
 * <p>Time: 23:06
 * <p>===================================</p>
 *
 */
class PacketCreateDatabase constructor(val name: String = "") : PacketOut
