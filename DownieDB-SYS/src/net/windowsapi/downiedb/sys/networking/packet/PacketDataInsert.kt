package net.windowsapi.downiedb.sys.networking.packet

/**
 *
 * <p>===================================</p>
 * <p>Project: DownieDB</p>
 * <p>Package: net.windowsapi.downiedb.networking.packet</p>
 * <p>File: PacketDataInsert</p>
 * <p>===================================</p>
 * <p>Author: Robin F.
 * <p>===================================</p>
 * <p>Date: 20.08.2017
 * <p>Time: 23:20
 * <p>===================================</p>
 *
 */
class PacketDataInsert constructor(val databaseName: String = "", val key: String = "", val toInsert: Any = "", val override: Boolean = false) : PacketOut
