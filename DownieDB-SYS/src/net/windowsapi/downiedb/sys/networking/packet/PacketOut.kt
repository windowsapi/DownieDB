package net.windowsapi.downiedb.sys.networking.packet

/**
 *
 * <p>===================================</p>
 * <p>Project: DownieDB</p>
 * <p>Package: net.windowsapi.downiedb.networking.packet</p>
 * <p>File: PacketOut</p>
 * <p>===================================</p>
 * <p>Author: Robin F.
 * <p>===================================</p>
 * <p>Date: 22.08.2017
 * <p>Time: 12:59
 * <p>===================================</p>
 *
 */
interface PacketOut : Packet