import net.windowsapi.downiedb.sys.networking.packet.Packet

/**
 *
 * <p>===================================</p>
 * <p>Project: DownieDB</p>
 * <p>Package: net.windowsapi.downiedb.networking.packet</p>
 * <p>File: PacketIn</p>
 * <p>===================================</p>
 * <p>Author: Robin F.
 * <p>===================================</p>
 * <p>Date: 22.08.2017
 * <p>Time: 12:56
 * <p>===================================</p>
 *
 */
interface PacketIn : Packet