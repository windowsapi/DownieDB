package net.windowsapi.downiedb.sys.networking.packet

/**
 *
 * <p>===================================</p>
 * <p>Project: DownieDB</p>
 * <p>Package: net.windowsapi.downiedb.networking.packet</p>
 * <p>File: PacketDataRequest</p>
 * <p>===================================</p>
 * <p>Author: Robin F.
 * <p>===================================</p>
 * <p>Date: 20.08.2017
 * <p>Time: 23:13
 * <p>===================================</p>
 *
 */
class PacketDataRequest constructor(val databaseName: String = "", val key: String = "") : PacketOut
