package net.windowsapi.downiedb.sys.storage

import net.windowsapi.downiedb.sys.misc.Constants
import java.io.*

/**
 *
 * <p>===================================</p>
 * <p>Project: DownieDB</p>
 * <p>Package: net.windowsapi.downiedb.storage</p>
 * <p>File: Configuration</p>
 * <p>===================================</p>
 * <p>Author: Robin F.
 * <p>===================================</p>
 * <p>Date: 20.08.2017
 * <p>Time: 15:34
 * <p>===================================</p>
 *
 */
class Configuration constructor(name: String) : Storageable {

    private val map = mutableMapOf<String, Any>()
    private val defaults = mutableMapOf<String, Any>()

    private val file = File("${Constants.CONFIGURATION_DIRECTORY.name}/$name.cfg")

    init {
        if (!file.parentFile.exists())
            file.parentFile.mkdirs()

        if (!file.exists())
            file.createNewFile()

        load()
    }

    fun store(key: String, toStore: Any): Int {
        return store(key, toStore, false)
    }

    fun store(key: String, toStore: Any, override: Boolean): Int {
        if (map.containsKey(key)) {
            if (override)
                map.replace(key, toStore)
        } else
            map.put(key, toStore)

        return Constants.CODE_SUCCESS
    }

    fun get(key: String): Any {
        return map.getOrDefault(key, Constants.CODE_NOT_FOUND)
    }

    fun getString(key: String): String {
        return get(key).toString()
    }

    fun getByte(key: String): Byte {
        return getString(key).toByte()
    }

    fun getShort(key: String): Short {
        return getString(key).toShort()
    }

    fun getInt(key: String): Int {
        return getString(key).toInt()
    }

    fun getLong(key: String): Long {
        return getString(key).toLong()
    }

    fun getFloat(key: String): Float {
        return getString(key).toFloat()
    }

    fun getDouble(key: String): Double {
        return getString(key).toDouble()
    }

    fun getBoolean(key: String): Boolean {
        return getString(key).toBoolean()
    }

    fun load() {
        map.clear()

        val reader = BufferedReader(FileReader(file))
        var line = reader.readLine()

        while (line != null) {
            val splitted = line.split(Constants.SPACER)

            val key = splitted[0]
            val value = splitted[1]

            store(key, value)

            line = reader.readLine()
        }

        reader.close()

        transferDefaults()
    }

    fun save() {
        transferDefaults()

        file.delete()
        file.createNewFile()

        val writer = BufferedWriter(FileWriter(file))

        for ((key, value) in map) {
            writer.write("$key${Constants.SPACER}$value")
            writer.newLine()
        }

        writer.flush()

        writer.close()
    }

    private fun transferDefaults() {
        for ((key, value) in defaults)
            if (get(key) == Constants.CODE_NOT_FOUND)
                store(key, value)
    }

    override fun toString(): String {
        val builder = StringBuilder()

        for ((key, value) in map)
            builder.append("$key : $value\n")

        return builder.toString()
    }

    fun default(key: String, toStore: Any): Int {
        if (defaults.containsKey(key))
            defaults.replace(key, toStore)
        else
            defaults.put(key, toStore)

        transferDefaults()

        return Constants.CODE_SUCCESS
    }
}
