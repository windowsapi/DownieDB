package net.windowsapi.downiedb.sys.misc

import java.io.File

/**
 *
 * <p>===================================</p>
 * <p>Project: DownieDB</p>
 * <p>Package: net.windowsapi.downiedb.misc</p>
 * <p>File: Constants</p>
 * <p>===================================</p>
 * <p>Author: Robin F.
 * <p>===================================</p>
 * <p>Date: 20.08.2017
 * <p>Time: 03:45
 * <p>===================================</p>
 *
 */
object Constants {

    val CODE_SUCCESS = 0x0
    val CODE_NOT_FOUND = 0x1
    val CODE_DATABASE_ALREADY_EXITS = 0x2

    val SPACER = "="

    val DATABASE_DIRECTORY = File("data/")
    val CONFIGURATION_DIRECTORY = File("base/")

}
