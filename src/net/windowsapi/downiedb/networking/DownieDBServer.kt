package net.windowsapi.downiedb.networking

import com.esotericsoftware.kryonet.Server
import net.windowsapi.downiedb.storage.DownieDatabase
import net.windowsapi.downiedb.sys.misc.Constants
import net.windowsapi.downiedb.sys.networking.packet.*
import java.io.File

/**
 *
 * <p>===================================</p>
 * <p>Project: DownieDB</p>
 * <p>Package: net.windowsapi.downiedb.server</p>
 * <p>File: DownieDBServer</p>
 * <p>===================================</p>
 * <p>Author: Robin F.
 * <p>===================================</p>
 * <p>Date: 20.08.2017
 * <p>Time: 22:51
 * <p>===================================</p>
 *
 */
class DownieDBServer constructor(port: Int) {

    private val server = Server()
    val databases = mutableMapOf<String, DownieDatabase>()

    init {
        server.start()
        server.bind(port)

        server.kryo.register(Packet::class.java)
        server.kryo.register(PacketCreateDatabase::class.java)
        server.kryo.register(PacketDataInsert::class.java)
        server.kryo.register(PacketDataRequest::class.java)
        server.kryo.register(PacketDataResult::class.java)
        server.kryo.register(PacketResultCode::class.java)

        server.addListener(DownieDBServerListener(this))

        loadDatabases()
    }

    fun createDatabase(name: String): DownieDatabase? {
        if (!databases.containsKey(name)) {
            val result = DownieDatabase(name)
            loadDatabases()
        }

        return null;
    }

    fun getDatabase(name: String): DownieDatabase? {
        return databases[name]
    }

    private fun loadDatabases() {
        val files: Array<out File> = Constants.DATABASE_DIRECTORY.listFiles() ?: return

        databases.clear()

        files.forEach { file ->
            val name = file.nameWithoutExtension

            println("Loading Database $name..")

            databases.put(name, DownieDatabase(name))

            println("Loaded Database $name")
            println()
        }
    }

    override fun toString(): String {
        val builder = StringBuilder()

        for ((key, value) in databases)
            builder.append("$key : $value")

        return builder.toString()
    }
}
