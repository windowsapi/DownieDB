package net.windowsapi.downiedb.networking

import com.esotericsoftware.kryonet.Connection
import com.esotericsoftware.kryonet.Listener
import net.windowsapi.downiedb.storage.DownieDatabase
import net.windowsapi.downiedb.sys.misc.Constants
import net.windowsapi.downiedb.sys.networking.packet.*

/**
 *
 * <p>===================================</p>
 * <p>Project: DownieDB</p>
 * <p>Package: net.windowsapi.downiedb.networking</p>
 * <p>File: DownieDBServerListener</p>
 * <p>===================================</p>
 * <p>Author: Robin F.
 * <p>===================================</p>
 * <p>Date: 20.08.2017
 * <p>Time: 23:06
 * <p>===================================</p>
 *
 */
class DownieDBServerListener constructor(val server: DownieDBServer) : Listener() {

    override fun received(connection: Connection?, received: Any?) {
        if (received is Packet) {

            if (received is PacketCreateDatabase) {
                val created: DownieDatabase? = server.createDatabase(received.name)

                if (created == null)
                    connection?.sendTCP(PacketResultCode(Constants.CODE_DATABASE_ALREADY_EXITS))
                else
                    connection?.sendTCP(PacketResultCode(Constants.CODE_SUCCESS))
            }

            if (received is PacketDataRequest) {
                val databaseName = received.databaseName
                val key = received.key

                val database = server.getDatabase(databaseName)

                if (database == null)
                    connection?.sendTCP(PacketResultCode(Constants.CODE_NOT_FOUND))
                else {
                    connection?.sendTCP(PacketDataResult(database.get(key)))
                    connection?.sendTCP(PacketResultCode(Constants.CODE_SUCCESS))
                }
            }

            if (received is PacketDataInsert) {
                val databaseName = received.databaseName
                val key = received.key
                val value = received.toInsert
                val override = received.override

                val database = server.getDatabase(databaseName)

                if (database == null)
                    connection?.sendTCP(PacketResultCode(Constants.CODE_NOT_FOUND))
                else {
                    database.store(key, value, override)
                    database.save()

                    connection?.sendTCP(PacketResultCode(Constants.CODE_SUCCESS))
                }
            }
        }
    }
}
