package net.windowsapi.downiedb

import net.windowsapi.downiedb.networking.DownieDBServer
import net.windowsapi.downiedb.sys.storage.Configuration
import java.util.*

/**
 *
 * <p>===================================</p>
 * <p>Project: DownieDB</p>
 * <p>Package: net.windowsapi.downiedb</p>
 * <p>File: main</p>
 * <p>===================================</p>
 * <p>Author: Robin F.
 * <p>===================================</p>
 * <p>Date: 20.08.2017
 * <p>Time: 03:39
 * <p>===================================</p>
 *
 */
fun main(args: Array<String>) {
    val prefs = Configuration("config")

    prefs.default("hash-salt", UUID.randomUUID())
    prefs.default("auth", false)
    prefs.default("password", UUID.randomUUID().toString().replace("-", "").toUpperCase())
    prefs.default("port", 17553)
    prefs.default("web-port", 17554)
    prefs.default("encryption", "DES")

    prefs.save()

    DownieDBServer(prefs.getInt("port"))
}
