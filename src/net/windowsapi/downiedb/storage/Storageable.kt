package net.windowsapi.downiedb.storage

/**
 *
 * <p>===================================</p>
 * <p>Project: DownieDB</p>
 * <p>Package: net.windowsapi.downiedb.storage</p>
 * <p>File: Storageable</p>
 * <p>===================================</p>
 * <p>Author: Robin F.
 * <p>===================================</p>
 * <p>Date: 21.08.2017
 * <p>Time: 01:41
 * <p>===================================</p>
 *
 */
interface Storageable